var mysql = require('mysql');
var bcrypt = require('bcryptjs');

module.exports ={

    getSignUp : function(req,res,next){
        return res.render('users/signup');
    },

    postSignUp : function(req,res,next){
        //  console.log(req.body);
        //  return;
        var salt = bcrypt.genSaltSync(10);
        var password = bcrypt.hashSync(req.body.password, salt);
        var user = {
            email : req.body.email,
            password : password
        };
        var config =require('.././database/config');
        var db = mysql.createConnection(config);

        //conectamos la DB

        db.connect();

        // insertamos los valores enviados desde el formulario
        db.query('INSERT INTO usuarios SET ?', user, function(err, rows, fields){
        if(err) throw err;
            db.end();
        });
        req.flash('info', 'Se ha registrado exitosamente, Ya puedes Iniciar Sesión ADMINISTRADOR :)');

        return res.redirect('/auth/signin');
    },
    getSignIn : function(req, res, next){
        return res.render('users/signin', {message : req.flash('info'), authmessage: req.flash('authmessage')});
    },

    logout : function(req, res, next){
        req.logout();
        res.redirect('/auth/signin');
    },

    getUserPanel : function(req, res, next){
        res.render('users/panel',
        {
            isAuthenticated : req.isAuthenticated(),
            user : req.user
        });
    },

    

    getPrincipal : (req, res, next) => {
        var config =require('.././database/config');
        var db = mysql.createConnection(config);

        //conectamos la DB

        db.connect();

        // insertamos los valores enviados desde el formulario
        db.query('SELECT * , (SELECT regiones.Nombre from regiones WHERE regiones.idRegion = deudores.idRegion) AS Region,  (SELECT pagos.Deuda FROM pagos WHERE pagos.idDeudor = deudores.idDeudor ) AS Deudas , (SELECT pagos.FechaDeudaActual pagos FROM pagos WHERE pagos.idDeudor = deudores.idDeudor ) AS FechaD , (SELECT pagos.FechaUltimoPago pagos FROM pagos WHERE pagos.idDeudor = deudores.idDeudor ) AS FechaV FROM deudores',  (err, rows) => {

            if(err){
                console.log(err);
                // res.render('error');
            }else{
                res.render('users/deudor', {

                    isAuthenticated : req.isAuthenticated(),
                    user : req.user,
                    data : rows

                });

                console.log(rows);
            }

        });
},

      getSucursal : (req, res, next) => {

          var config =require('.././database/config');
          var db = mysql.createConnection(config);

            //conectamos la DB

          db.connect();

            // insertamos los valores enviados desde el formulario
          db.query('SELECT Nombre, Direccion, Telefono, Horario, (SELECT regiones.Nombre from regiones WHERE regiones.idRegion = sucursales.idRegion) AS Region FROM sucursales',  (err, rows) => {

              if(err){
                console.log(err);
                //   res.render('error');
              }else{
                  res.render('users/sucursal', {

                      isAuthenticated : req.isAuthenticated(),
                      user : req.user,
                      data : rows

                    

                  });
                  console.log(rows);
              }

          });
},

getCpagos : (req, res, next) => {

    var config =require('.././database/config');
    var db = mysql.createConnection(config);

      //conectamos la DB

    db.connect();

      // insertamos los valores enviados desde el formulario
    db.query('SELECT (SELECT deudores.Nombre from deudores WHERE deudores.idDeudor = pagos.idDeudor) AS Deudor, deuda, DeudaActual, FechaDeudaActual, FechaProximoPago, FechaUltimoPago, (SELECT sucursales.Nombre from sucursales WHERE sucursales.idSucursal = pagos.idSucursal) AS Sucursales, Meses, pago, Intereses, Monto, SaldosInsolutos FROM pagos',  (err, rows) => {

        if(err){
          console.log(err);
          //   res.render('error');
        }else{
            res.render('users/ConsultaPagos', {

                isAuthenticated : req.isAuthenticated(),
                user : req.user,
                data : rows
            });
            console.log(rows);
        }

    });
},

getCpagos : (req, res, next) => {

    var config =require('.././database/config');
    var db = mysql.createConnection(config);

      //conectamos la DB

    db.connect();

      // insertamos los valores enviados desde el formulario
    db.query('SELECT (SELECT deudores.Nombre from deudores WHERE deudores.idDeudor = pagos.idDeudor) AS Deudor, deuda, DeudaActual, FechaDeudaActual, FechaProximoPago, FechaUltimoPago, (SELECT sucursales.Nombre from sucursales WHERE sucursales.idSucursal = pagos.idSucursal) AS Sucursales, Meses, pago, Intereses, Monto, SaldosInsolutos FROM pagos',  (err, rows) => {

        if(err){
          console.log(err);
          //   res.render('error');
        }else{
            res.render('users/ConsultaPagos', {

                isAuthenticated : req.isAuthenticated(),
                user : req.user,
                data : rows
            });
            console.log(rows);
        }

    });
},

getEliminarAg : function(req, res, next){
    let idA = req.params.idAgente
    console.log(idA);

    var config =require('.././database/config');
    var db = mysql.createConnection(config);
    
    //conectamos la DB
    
    db.connect();

    db.query('DELETE * FROM agentes WHERE idAgente = ? ', idA, (err, rows) => {
        if(err){
            res.render('error');
        }else{
            res.render('users/panel',
            {
                isAuthenticated : req.isAuthenticated(),
                user : req.user,
                data : rows
            });
            console.log(rows);
        }

    });
    
    console.log('DONE');
},

postEliminarAg : function(req, res, next){
    let datos = {
        email : req.body.email,
        nombre : req.body.nombre,
        telefono : req.body.telefono,
        domicilio : req.body.direccion
    }



    var config =require('.././database/config');
    var db = mysql.createConnection(config);
    
    // conectamos la DB
    
    db.connect();

    //console.log(datos);

    db.query('SELECT idAgente FROM Agentes WHERE idAgente = ?', datos.idAgente, (err, rows) => {
            if(err) {
                  res.render(err);
            }else{

                    if(rows.length == 1){
                        db.query('UPDATE Agentes SET ? WHERE idAgente = ? ', [datos, datos.idAgente],(err, rows) => {
                                if(err){
                                    let numerosA = {
                                        title : `ERROR AL INSERTAR`,
                                        description : 'ERROR EN LA SINTAXIS',
                                        error : err
                                    }
                    
                                    res.render('../error', numerosA);
                                }else{
                                    res.render('home',
                                        {
                                        isAuthenticated : req.isAuthenticated(),
                                        user : req.user
                                        });
                                    
                                    console.log('SE ACTUALIZO');
                                }
                        });
                    }else{
                        db.query('INSERT INTO Agentes SET ?', datos,(err, rows) => {
                                if(err){
                                    let numerosA = {
                                        title : `ERROR AL INSERTAR`,
                                        description : 'ERROR EN LA SINTAXIS',
                                        error : err
                                    }
                    
                                    res.render('../error', numerosA);
                                }else{
                                    res.render('home',
                                        {
                                        isAuthenticated : req.isAuthenticated(),
                                        user : req.user
                                        });
                                    
                                    console.log('SE INSERTO');
                                }
                        });
                    }

                }
    });


},


getAgente : (req, res, next) => {

    var config =require('.././database/config');
    var db = mysql.createConnection(config);

      //conectamos la DB

    db.connect();

      // insertamos los valores enviados desde el formulario
    db.query('SELECT * FROM agentes',  (err, rows) => {

        if(err){
            console.log(err);
            // res.render('error');
        }else{
            res.render('users/agentes', {

                isAuthenticated : req.isAuthenticated(),
                user : req.user,
                data : rows

            });
            console.log(rows);
        }

    });
},

getUpAgente : (req, res, next) => {

    var config =require('.././database/config');
    var db = mysql.createConnection(config);

      //conectamos la DB

    db.connect();

      // insertamos los valores enviados desde el formulario
    db.query('SELECT idAgente, Nombre FROM agentes',  (err, rows) => {

        if(err){
            console.log(err);
            // res.render('error');
        }else{
            res.render('users/UpAgente', {

                isAuthenticated : req.isAuthenticated(),
                user : req.user,
                data : rows

            });
            console.log(rows);
        }

    });
},

postUpAgente : (req, res, next) => {
    var nsuc = {
        email : req.body.email,
        nombre : req.body.nombre,
        telefono : req.body.telefono,
        domicilio : req.body.direccion

    };
    var config =require('.././database/config');
    var db = mysql.createConnection(config);
    
    //conectamos la DB
    
    db.connect();

    db.query('INSERT INTO agentes SET ?', nsuc,(err, rows) => {
        if(err){
            let sucu = {
                title : `ERROR AL INSERTAR`,
                description : 'ERROR EN LA SINTAXIS',
                error : err
            }

            res.render('error', sucu);
        }else{
            res.render('users/panel',
                {
                isAuthenticated : req.isAuthenticated(),
                user : req.user,
                data : rows
                });
            
            console.log('DONE');
        }
    });
    console.log(nsuc);
    //res.redirect('/panel');
},

getUpAgente2 : (req, res, next) => {

    var config =require('.././database/config');
    var db = mysql.createConnection(config);

      //conectamos la DB

    db.connect();

      // insertamos los valores enviados desde el formulario
    db.query('SELECT idAgente, Nombre FROM agentes',  (err, rows) => {

        if(err){
            console.log(err);
            // res.render('error');
        }else{
            res.render('users/UpAgente2', {

                isAuthenticated : req.isAuthenticated(),
                user : req.user,
                data : rows

            });
            console.log(rows);
        }

    });
},

postUpAgente2 : (req, res, next) => {
    var nsuc = {
        email : req.body.email,
        nombre : req.body.nombre,
        telefono : req.body.telefono,
        domicilio : req.body.direccion

    };
    var config =require('.././database/config');
    var db = mysql.createConnection(config);
    
    //conectamos la DB
    
    db.connect();

    db.query('INSERT INTO agentes SET ?', nsuc,(err, rows) => {
        if(err){
            let sucu = {
                title : `ERROR AL INSERTAR`,
                description : 'ERROR EN LA SINTAXIS',
                error : err
            }

            res.render('error', sucu);
        }else{
            res.render('users/panel',
                {
                isAuthenticated : req.isAuthenticated(),
                user : req.user,
                data : rows
                });
            
            console.log('DONE');
        }
    });
    console.log(nsuc);
    //res.redirect('/panel');
},

getNsucursal : (req, res, next) => {

    var config =require('.././database/config');
    var db = mysql.createConnection(config);

      //conectamos la DB

    db.connect();

      // insertamos los valores enviados desde el formulario
    db.query('SELECT idRegion, Nombre FROM regiones',  (err, rows) => {

        if(err){
            console.log(err);
            // res.render('error');
        }else{
            res.render('users/Nsucursaless', {

                isAuthenticated : req.isAuthenticated(),
                user : req.user,
                data : rows

            });
            console.log(rows);
        }

    });
},

postNsucursal : (req, res, next) => {
    var nsuc = {
        Nombre : req.body.nombre,
        Idregion : req.body.region,
        Telefono : req.body.telefono,
        Horario : req.body.horario,
        Direccion : req.body.direccion

    };
    var config =require('.././database/config');
    var db = mysql.createConnection(config);
    
    //conectamos la DB
    
    db.connect();

    db.query('INSERT INTO sucursales SET ?', nsuc,(err, rows) => {
        if(err){
            let sucu = {
                title : `ERROR AL INSERTAR`,
                description : 'ERROR EN LA SINTAXIS',
                error : err
            }

            res.render('error', sucu);
        }else{
            res.render('users/panel',
                {
                isAuthenticated : req.isAuthenticated(),
                user : req.user,
                data : rows
                });
            
            console.log('DONE');
        }
    });
    console.log(nsuc);
},

getUpSucursal : (req, res, next) => {

    var config =require('.././database/config');
    var db = mysql.createConnection(config);

      //conectamos la DB

    db.connect();

      // insertamos los valores enviados desde el formulario
    db.query('SELECT idRegion, Nombre FROM regiones',  (err, rows) => {

        if(err){
            console.log(err);
            // res.render('error');
        }else{
            res.render('users/UpSucursales', {

                isAuthenticated : req.isAuthenticated(),
                user : req.user,
                data : rows

            });
            console.log(rows);
        }

    });
},

postUpSucursal : (req, res, next) => {
    var nsuc = {
        Nombre : req.body.nombre,
        Idregion : req.body.region,
        Telefono : req.body.telefono,
        Horario : req.body.horario,
        Direccion : req.body.direccion

    };
    var config =require('.././database/config');
    var db = mysql.createConnection(config);
    
    //conectamos la DB
    
    db.connect();

    db.query('INSERT INTO sucursales SET ?', nsuc,(err, rows) => {
        if(err){
            let sucu = {
                title : `ERROR AL INSERTAR`,
                description : 'ERROR EN LA SINTAXIS',
                error : err
            }

            res.render('error', sucu);
        }else{
            res.render('users/panel',
                {
                isAuthenticated : req.isAuthenticated(),
                user : req.user,
                data : rows
                });
            
            console.log('DONE');
        }
    });
    console.log(nsuc);
},

getNagente : function(req, res, next){
    res.render('users/Nagentes',
    {
        isAuthenticated : req.isAuthenticated(),
        user : req.user,
        
    });

},


postNagente : (req, res, next) => {
    var nagen = {
        email : req.body.email,
        nombre : req.body.nombre,
        telefono : req.body.telefono,
        domicilio : req.body.direccion

    };
    var config =require('.././database/config');
    var db = mysql.createConnection(config);
    
    //conectamos la DB
    
    db.connect();

    db.query('INSERT INTO agentes SET ?', nagen,(err, rows) => {
        if(err){
            let agente = {
                title : `ERROR AL INSERTAR`,
                description : 'ERROR EN LA SINTAXIS',
                error : err
            }

            res.render('error', agente);
        }else{
            res.render('users/panel',
                {
                isAuthenticated : req.isAuthenticated(),
                user : req.user,
                data : rows
                });
            
            console.log('DONE');
        }
    });
    console.log(nagen);
    //res.redirect('/panel');
},

getNdeudor : (req, res, next) => {

    var config =require('.././database/config');
    var db = mysql.createConnection(config);

      //conectamos la DB

    db.connect();

      // insertamos los valores enviados desde el formulario
    db.query('SELECT idRegion, Nombre FROM regiones',  (err, rows) => {

        if(err){
            console.log(err);
            // res.render('error');
        }else{
            res.render('users/Ndeudores', {

                isAuthenticated : req.isAuthenticated(),
                user : req.user,
                data : rows

            });
            console.log(rows);
        }

    });
},



postNdeudor : (req, res, next) => {
    var ndeu = {
        nombre : req.body.nombre,
        NumCliente: req.body.nCliente,
        RFC: req.body.rfc,
        Idregion : req.body.region,
        TelefonoMovil : req.body.telefonoMovil,
        TelefonoContacto : req.body.telefonoContacto,
        Referencias : req.body.referencias

    };
    var config =require('.././database/config');
    var db = mysql.createConnection(config);
    
    //conectamos la DB
    
    db.connect();

    db.query('INSERT INTO deudores SET ?', ndeu,(err, rows) => {
        if(err){
            let deudor = {
                title : `ERROR AL INSERTAR`,
                description : 'ERROR EN LA SINTAXIS',
                error : err
            }

            res.render('error', deudor);
        }else{
            res.render('users/panel',
                {
                isAuthenticated : req.isAuthenticated(),
                user : req.user,
                data : rows
                });
            
            console.log('DONE');
        }
    });
    console.log(ndeu);
//    res.redirect('/pan');
},



getUpdeudor : (req, res, next) => {

    var config =require('.././database/config');
    var db = mysql.createConnection(config);

      //conectamos la DB

    db.connect();

      // insertamos los valores enviados desde el formulario
    db.query('SELECT idRegion, Nombre FROM regiones',  (err, rows) => {

        if(err){
            console.log(err);
            // res.render('error');
        }else{
            res.render('users/UpDeudor', {

                isAuthenticated : req.isAuthenticated(),
                user : req.user,
                data : rows

            });
            console.log(rows);
        }

    });
},



postGetdeudor : (req, res, next) => {
    var ndeu = {
        nombre : req.body.nombre,
        NumCliente: req.body.nCliente,
        RFC: req.body.rfc,
        Idregion : req.body.region,
        TelefonoMovil : req.body.telefonoMovil,
        TelefonoContacto : req.body.telefonoContacto,
        Referencias : req.body.referencias

    };
    var config =require('.././database/config');
    var db = mysql.createConnection(config);
    
    //conectamos la DB
    
    db.connect();

    db.query('INSERT INTO deudores SET ?', ndeu,(err, rows) => {
        if(err){
            let deudor = {
                title : `ERROR AL INSERTAR`,
                description : 'ERROR EN LA SINTAXIS',
                error : err
            }

            res.render('error', deudor);
        }else{
            res.render('users/panel',
                {
                isAuthenticated : req.isAuthenticated(),
                user : req.user,
                data : rows
                });
            
            console.log('DONE');
        }
    });
    console.log(ndeu);
//    res.redirect('/pan');
},






getActualizarDeudor: function(req, res, next){
    let idD = req.params.idDeudor;
    console.log(idD);
    var config =require('.././database/config');
    var db = mysql.createConnection(config);
    
    //conectamos la DB
    
    db.connect();

    db.query('SELECT * FROM deudores WHERE idDeudor = ? ', idD, (err, rows) => {
        if(err){
            res.render('error');
        }else{
            res.render('users/Adeudor',
            {
                isAuthenticated : req.isAuthenticated(),
                user : req.user,
                data : rows
            });
            console.log(rows);
        }

    });

},

postActualizarDeudor : function(req, res, next){
    let actuaDeu = {
        nombre : req.body.nombre,
        NumCliente: req.body.nCliente,
        RFC: req.body.rfc,
        Idregion : req.body.region,
        TelefonoMovil : req.body.telefonoMovil,
        TelefonoContacto : req.body.telefonoContacto,
        Referencias : req.body.referencias
    }

    /*var config =require('.././database/config');
    var db = mysql.createConnection(config);
    
    // // conectamos la DB
    
    db.connect();

    //console.log(actuaNum);
    db.query('SELECT idDeudor FROM deudores WHERE idDeudor = ?', actuaDeu.idDeudor, (err, rows) => {
        if(err) {
              res.render(err);
        }else{

            if(rows.length == 1){
                db.query('UPDATE deudores SET ? WHERE idDeudor = ? ', [actuaDeu, actuaDeu.idDeudor],(err, rows) => {
                    if(err){
                        let Adeudor = {
                            title : `ERROR AL INSERTAR`,
                            description : 'ERROR EN LA SINTAXIS',
                            error : err
                        }
        
                        res.render('../error', Adeudor);
                    }else{
                        res.render('home');
                        
                        console.log('SE ACTUALIZO');
                    }
                });
            }else{
                db.query('INSERT INTO deudores SET ?', actuaDeu,(err, rows) => {
                    if(err){
                        let Adeudor = {
                            title : `ERROR AL INSERTAR`,
                            description : 'ERROR EN LA SINTAXIS',
                            error : err
                        }

                        res.render('../error', Adeudor);
                    }else{
                        res.render('home');

                        
                        console.log('SE INSERTO');
                    }
                });
            }
        }

    });*/

},

getMes : function(req, res, next){
    let idA = req.params.idAgente;
    console.log(idA);
    var config =require('.././database/config');
    var db = mysql.createConnection(config);
    
    //conectamos la DB
    
    db.connect();

    db.query('SELECT * FROM sesiones WHERE Fecha LIKE ?' , '%12%'
    , (err, rows) => {

        if(err){
            res.render('error');
        }else{

            console.log(rows);
            res.render('users/reportemes',
            {
                isAuthenticated : req.isAuthenticated(),
                user : req.user,
                data : rows
            });
        }
    });
    // res.render('users/reportemes',
    // {
    //     isAuthenticated : req.isAuthenticated(),
    //     user : req.user
    // });
},

postMes : function (req, res, next) {

    let datosMes = {
        Fecha : req.body.fecha
    }

    var config =require('.././database/config');
    var db = mysql.createConnection(config);
    
    //conectamos la DB
    
    db.connect();
    db.query('SELECT * FROM capturasesion WHERE Fecha LIKE  ?', req.Fecha, (err, rows) => {
        if(err){
            res.render('error');
            console.log("No hay registros de esa fecha");
        }else{
            res.redirect('reportemes');
            console.log(datosMes);
        }
    });
    console.log(datosMes);
    res.redirect('panel');
},
getDia : function(req, res, next){

    var config =require('.././database/config');
    var db = mysql.createConnection(config);
    
    //conectamos la DB
    
    db.connect();

    db.query('SELECT * FROM sesiones WHERE Fecha LIKE ?' , '%6%'
    , (err, rows) => {

        if(err){
            res.render('error');
        }else{

            console.log(rows);
            res.render('users/reportedias',
            {
                isAuthenticated : req.isAuthenticated(),
                user : req.user,
                data : rows
            });
        }
    });

    // res.render('users/reportedia',
    // {
    //     isAuthenticated : req.isAuthenticated(),
    //     user : req.user
    // });
},

postDia : function (req, res, next) {

    let datosDia = {
        Fecha : req.body.fecha
    }

    var config =require('.././database/config');
    var db = mysql.createConnection(config);
    
    //conectamos la DB
    
    db.connect();
    db.query('SELECT * FROM capturasesion WHERE Fecha LIKE  ?', datosDia.Fecha, (err, rows) => {
        if(err){
            res.render('error');
            console.log("No hay registros de esa fecha");
        }else{
            res.redirect('reportemes');
            console.log(datosDia);
        }
    });

    console.log(datosDia);
    res.redirect('panel');
},
    getConsulta : function(req, res, next){
        res.render('users/consulta',
        {
            isAuthenticated : req.isAuthenticated(),
            user : req.user
        });
    },

    getSesion : function(req, res, next){
        res.render('users/pagos',
        {
            isAuthenticated : req.isAuthenticated(),
            user : req.user
        });
    }

    

};
