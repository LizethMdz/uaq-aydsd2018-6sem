var express = require('express');
var router = express.Router();
var controllers = require('.././controllers');
var passport = require('passport')	
var AuthMiddleware = require('.././middleware/auth');

router.get('/', controllers.HomeController.index);
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* Routes de usuario */
router.get('/auth/signup', controllers.UserController.getSignUp);
	
router.post('/auth/signup', controllers.UserController.postSignUp);
 	
router.get('/auth/signin', controllers.UserController.getSignIn);

router.post('/auth/signin', passport.authenticate('local',{
  successRedirect: '/users/panel',
  failureRedirect:'/auth/signin',
  failureFlash: true
}));




router.get('/auth/logout', controllers.UserController.logout);
router.get('/users/panel', AuthMiddleware.isLogged, controllers.UserController.getUserPanel);
router.get('/users/deudor', AuthMiddleware.isLogged, controllers.UserController.getPrincipal);
router.get('/users/consulta', AuthMiddleware.isLogged, controllers.UserController.getConsulta);
router.get('/users/sesiones', AuthMiddleware.isLogged, controllers.UserController.getSesion);
router.get('/users/sucursal', AuthMiddleware.isLogged, controllers.UserController.getSucursal);
router.get('/users/agentes', AuthMiddleware.isLogged, controllers.UserController.getAgente);
router.get('/users/ConsultaPagos', AuthMiddleware.isLogged, controllers.UserController.getCpagos);


router.get('/users/Nsucursaless', AuthMiddleware.isLogged, controllers.UserController.getNsucursal);
router.get('/users/Nagentes', AuthMiddleware.isLogged, controllers.UserController.getNagente);
router.get('/users/Ndeudores', AuthMiddleware.isLogged, controllers.UserController.getNdeudor);
router.get('/users/Adeudor', AuthMiddleware.isLogged, controllers.UserController.getActualizarDeudor);

router.get('/users/agentes', AuthMiddleware.isLogged, controllers.UserController.getEliminarAg);
router.get('/users/UpAgente', AuthMiddleware.isLogged, controllers.UserController.getUpAgente);
router.get('/users/UpAgente2', AuthMiddleware.isLogged, controllers.UserController.getUpAgente2);

router.get('/users/UpSucursales', AuthMiddleware.isLogged, controllers.UserController.getUpSucursal);


router.get('/users/UpDeudor', AuthMiddleware.isLogged, controllers.UserController.getUpdeudor);




router.post('/users/Nsucursaless', controllers.UserController.postNsucursal);
router.post('/users/Nagentes', controllers.UserController.postNagente);
router.post('/users/Ndeudores', controllers.UserController.postNdeudor);

router.post('/users/UpAgente', controllers.UserController.postUpAgente);
router.post('/users/UpAgente2', controllers.UserController.postUpAgente2);
router.post('/users/UpSucursales', controllers.UserController.postUpSucursal);

router.post('/users/UpDeudor', controllers.UserController.postGetdeudor);







/*REPORTES**/

router.get('/users/reportemes',  AuthMiddleware.isLogged, controllers.UserController.getMes);

router.get('/users/reportedias',  AuthMiddleware.isLogged, controllers.UserController.getDia);

router.post('/users/reportesMes', controllers.UserController.postMes);

router.post('/users/reportesDia', controllers.UserController.postDia)


module.exports = router;
