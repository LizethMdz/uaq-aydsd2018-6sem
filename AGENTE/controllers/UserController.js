var mysql = require('mysql');
var bcrypt = require('bcryptjs');
var nodemailer = require('nodemailer');

module.exports ={
 
    getSignUp : function(req,res,next){
        return res.render('users/signup');
    },
    	
    postSignUp : function(req,res,next){
        //  console.log(req.body);
        //  return;
        var salt = bcrypt.genSaltSync(10);
        var password = bcrypt.hashSync(req.body.password, salt);
        var user = {
            email : req.body.email,
            nombre : req.body.nombre,
            password : password,
            Domicilio : req.body.direccion,
            Telefono : req.body.telefono
        };
        var config =require('.././database/config');
        var db = mysql.createConnection(config);
        
        //conectamos la DB
        
        db.connect();
        
        // insertamos los valores enviados desde el formulario
        db.query('INSERT INTO agentes SET ?', user, function(err, rows, fields){
        if(err) throw err;
            db.end();
        });
        req.flash('info', 'Se ha registrado exitosamente, Ya puedes Iniciar Sesión :)');

        return res.redirect('/auth/signin');
    },
    getSignIn : function(req, res, next){
        return res.render('users/signin', {message : req.flash('info'), authmessage: req.flash('authmessage')});
    },

    logout : function(req, res, next){
        req.logout();
        res.redirect('/auth/signin');
    },

    getUserPanel : function(req, res, next){
        res.render('users/panel',
        {
            isAuthenticated : req.isAuthenticated(),
            user : req.user
        });
    },

    getPerfil : function(req, res, next){
        let idA = req.params.idAgente;
        console.log(idA);

        var config =require('.././database/config');
        var db = mysql.createConnection(config);
        
        //conectamos la DB
        
        db.connect();

        db.query('SELECT * FROM agentes WHERE idAgente = ? ', idA, (err, rows) => {
            if(err){
                res.render('error');
            }else{
                res.render('users/perfil',
                {
                    isAuthenticated : req.isAuthenticated(),
                    user : req.user,
                    data : rows
                });
                console.log(rows);
            }

        });
        // res.render('users/perfil',
        // {
        //     isAuthenticated : req.isAuthenticated(),
        //     user : req.user
        // });
    },

    postPerfil : (req, res, next) => {

        var IDAgente = req.body.agente;
        var perfil = {
            nombre : req.body.nombre,
            Domicilio : req.body.domicilio,
            Telefono : req.body.telcont,
        };

        console.log(perfil);
        var config =require('.././database/config');
        var db = mysql.createConnection(config);
        
        //conectamos la DB
        
        db.connect();

        db.query('UPDATE agentes SET ? WHERE idAgente =  ?', [perfil,IDAgente],(err, rows) => {
            if(err){
                let sesiones = {
                    title : `ERROR AL INSERTAR`,
                    description : 'ERROR EN LA SINTAXIS',
                    error : err
                }

                res.render('error', sesiones);
            }else{
                res.redirect('panel');
                
                console.log('UPDATE DONE');
            }
        });
        // return res.redirect('panel');
    },




    getPrincipal : (req, res, next) => {
        var config =require('.././database/config');
        var db = mysql.createConnection(config);
        
        //conectamos la DB
        
        db.connect();
        
        // Seleccionamos los valores de las tablas
        db.query('SELECT * , (SELECT regiones.Nombre from regiones WHERE regiones.idRegion = deudores.idRegion) AS Region, ' 
        + ' (SELECT pagos.Deuda FROM pagos WHERE pagos.idDeudor = deudores.idDeudor ) AS Deudas , ' 
        + ' (SELECT pagos.FechaDeudaActual pagos FROM pagos WHERE pagos.idDeudor = deudores.idDeudor ) AS FechaD, ' 
        + ' (SELECT TelefonoAnterior from numerohistorial WHERE numerohistorial.idDeudor = deudores.idDeudor) AS TelAnterior,'
        + ' (SELECT Fecha from numerohistorial WHERE numerohistorial.idDeudor = deudores.idDeudor) AS Fecha,'
        + ' (SELECT idAgente from numerohistorial WHERE numerohistorial.idDeudor = deudores.idDeudor) AS Agente,'
        + ' (SELECT NumeroVecesCorroborado from numerohistorial WHERE numerohistorial.idDeudor = deudores.idDeudor) AS VecesC,'
        + ' (SELECT Fecha from numeromodificado WHERE numeromodificado.idDeudor = deudores.idDeudor) AS FechaCambio,'
        + ' (SELECT TelefonoContacto from numeromodificado WHERE numeromodificado.idDeudor = deudores.idDeudor) AS TelefonoC,'
        + ' (SELECT Descripcion from numeromodificado WHERE numeromodificado.idDeudor = deudores.idDeudor) AS Descripcion,'
        + ' (SELECT pagos.FechaUltimoPago pagos FROM pagos WHERE pagos.idDeudor = deudores.idDeudor ) AS FechaV ' 
        + 'FROM deudores'
        ,  (err, rows) => {

            if(err){
                res.render('error');
            }else{
                res.render('users/home', {

                    isAuthenticated : req.isAuthenticated(),
                    user : req.user,
                    data : rows

                });

                //console.log(rows);
            }

        });

       

        
    },

    getConsulta : function(req, res, next){

        var config =require('.././database/config');
        var db = mysql.createConnection(config);
        
        //conectamos la DB
        
        db.connect();

        db.query('SELECT *, '
        + ' (SELECT TelefonoContacto from numeromodificado WHERE numeromodificado.idDeudor = deudores.idDeudor) AS TelefonoC, '
        + ' (SELECT TelefonoAnterior from numerohistorial WHERE numerohistorial.idDeudor = deudores.idDeudor) AS TelLeyenda ' 
        +' FROM deudores'
        , (err, rows) => {

            if(err){
                res.render('error');
            }else{

                console.log(rows);
                res.render('users/consulta',
                {
                    isAuthenticated : req.isAuthenticated(),
                    user : req.user,
                    data : rows
                });
            }
        });

    },

    getSesion : function(req, res, next){
        let idD = req.params.idDeudor
        console.log(idD);

        var config =require('.././database/config');
        var db = mysql.createConnection(config);
        
        //conectamos la DB
        
        db.connect();

        db.query('SELECT idDeudor FROM deudores WHERE idDeudor = ? ', idD, (err, rows) => {
            if(err){
                res.render('error');
            }else{
                res.render('users/sesiones',
                {
                    isAuthenticated : req.isAuthenticated(),
                    user : req.user,
                    data : rows
                });
                console.log(rows);
            }

        });
       

    },

    postSesion : (req, res, next) => {

        var sesionIniciada = {
            Fecha : req.body.fecha,
            Hora : req.body.hora,
            idAgente : req.body.agente,
            Comentarios : req.body.descripcion,
            idDeudor : req.body.cliente
        };
        var config =require('.././database/config');
        var db = mysql.createConnection(config);
        
        //conectamos la DB
        
         db.connect();

        db.query('INSERT INTO sesiones SET ?', sesionIniciada,(err, rows) => {
            if(err){
                let sesiones = {
                    title : `ERROR AL INSERTAR`,
                    description : 'ERROR EN LA SINTAXIS',
                    error : err
                }

                res.render('error', sesiones);
                console.log('NO SE INSERTO');
                console.log(error);
            }else{
                console.log('DONE');
                console.log('SE INSERTO');
                console.log(sesionIniciada);
                res.redirect('/users/consulta');
                
            
            }
        });


    },

    getEliminarNum : function(req, res, next){
        let idD = req.params.idDeudor
        console.log(idD);

        var config =require('.././database/config');
        var db = mysql.createConnection(config);
        
        //conectamos la DB
        
        db.connect();

        db.query('SELECT idDeudor, TelefonoMovil FROM deudores WHERE idDeudor = ? ', idD, (err, rows) => {
            if(err){
                res.render('error');
            }else{
                res.render('users/eliminarNum',
                {
                    isAuthenticated : req.isAuthenticated(),
                    user : req.user,
                    data : rows
                });
                console.log(rows);
            }

        });

    },

    postEliminarNum : function(req, res, next){
        let datos = {
            Fecha : req.body.fecha,
            TelefonoAnterior : req.body.telanterior,
            NumeroHistorial : req.body.tel,
            idDeudor : req.body.iddeudor,
            idAgente : req.body.agente,
            NumeroVecesCorroborado : req.body.corroboraciones
        }



        var config =require('.././database/config');
        var db = mysql.createConnection(config);
        
        // conectamos la DB
        
        db.connect();

        //console.log(datos);

        db.query('SELECT idDeudor FROM numerohistorial WHERE idDeudor = ?', datos.idDeudor, (err, rows) => {
                if(err) {
                      res.render(err);
                }else{

                        if(rows.length == 1){
                            db.query('UPDATE numerohistorial SET ? WHERE idDeudor = ? ', [datos, datos.idDeudor],(err, rows) => {
                                    if(err){
                                        let numerosA = {
                                            title : `ERROR AL INSERTAR`,
                                            description : 'ERROR EN LA SINTAXIS',
                                            error : err
                                        }
                        
                                        res.render('../error', numerosA);
                                    }else{
                                        res.render('home',
                                            {
                                            isAuthenticated : req.isAuthenticated(),
                                            user : req.user
                                            });
                                        
                                        console.log('SE ACTUALIZO');
                                    }
                            });
                        }else{
                            db.query('INSERT INTO numerohistorial SET ?', datos,(err, rows) => {
                                    if(err){
                                        let numerosA = {
                                            title : `ERROR AL INSERTAR`,
                                            description : 'ERROR EN LA SINTAXIS',
                                            error : err
                                        }
                        
                                        res.render('../error', numerosA);
                                    }else{
                                        res.render('home',
                                            {
                                            isAuthenticated : req.isAuthenticated(),
                                            user : req.user
                                            });
                                        
                                        console.log('SE INSERTO');
                                    }
                            });
                        }

                    }
        });


    },

    getActualizarNum: function(req, res, next){
        let idD = req.params.idDeudor;
        console.log(idD);
        var config =require('.././database/config');
        var db = mysql.createConnection(config);
        
        //conectamos la DB
        
        db.connect();

        db.query('SELECT idDeudor FROM deudores WHERE idDeudor = ? ', idD, (err, rows) => {
            if(err){
                res.render('error');
            }else{
                res.render('users/actualizarNum',
                {
                    isAuthenticated : req.isAuthenticated(),
                    user : req.user,
                    data : rows
                });
                console.log(rows);
            }

        });

    },

    postActualizarNum : function(req, res, next){
        let actuaNum = {
            Fecha : req.body.fecha,
            TelefonoContacto : req.body.telcont,
            idAgente : req.body.agente,
            Descripcion : req.body.descripcion,
            idDeudor : req.body.iddeudor
        }

        var config =require('.././database/config');
        var db = mysql.createConnection(config);
        
        // // conectamos la DB
        
        db.connect();

        //console.log(actuaNum);
        db.query('SELECT idDeudor FROM numeromodificado WHERE idDeudor = ?', actuaNum.idDeudor, (err, rows) => {
            if(err) {
                  res.render(err);
            }else{

                if(rows.length == 1){
                    db.query('UPDATE numeromodificado SET ? WHERE idDeudor = ? ', [actuaNum, actuaNum.idDeudor],(err, rows) => {
                        if(err){
                            let numerosA = {
                                title : `ERROR AL INSERTAR`,
                                description : 'ERROR EN LA SINTAXIS',
                                error : err
                            }
            
                            res.render('../error', numerosA);
                        }else{
                            res.render('home');
                            
                            console.log('SE ACTUALIZO');
                        }
                    });
                }else{
                    db.query('INSERT INTO numeromodificado SET ?', actuaNum,(err, rows) => {
                        if(err){
                            let numerosA = {
                                title : `ERROR AL INSERTAR`,
                                description : 'ERROR EN LA SINTAXIS',
                                error : err
                            }

                            res.render('../error', numerosA);
                        }else{
                            res.render('home');

                            
                            console.log('SE INSERTO');
                        }
                    });
                }
            }

        });

        

    },

    getGenerarPlan: function(req, res, next){
        let id = req.params.idDeudor;
        console.log(id);


        var config =require('.././database/config');
        var db = mysql.createConnection(config);
        
        //conectamos la DB
        
        db.connect();


        db.query('SELECT idRegion FROM deudores WHERE idDeudor = ?', id, (err, rows) => {
                if(err) {
                res.render('error');
                    
                }else{

                    if(rows.length == 1){

                        var data1 = rows;
                        console.log("encontrado");
                        console.log(data1);
                        //res.render('users/generarPagos');


                        db.query('SELECT * FROM sucursales WHERE idRegion = ? ', data1[0].idRegion, (err, rows) => {
                                if(err){
                                    res.render('error');
                                }else{
                    
                                    var dataSucursales = rows;
                                    console.log(dataSucursales);
                                    // res.render('users/generarPagos');
                                    db.query('SELECT * from pagos WHERE idDeudor =  ?', id, (err, rows) =>{
                                                    if(err){
                                                        res.render('error');
                                                    }else{
                                                        var dataPagos = rows;
                                
                                                        res.render('users/generarPagos',
                                                            {
                                                                isAuthenticated : req.isAuthenticated(),
                                                                user : req.user,
                                                                data : dataSucursales,
                                                                pagos : dataPagos
                                                            });
                                                            //console.log(dataSucursales, dataPagos);
                                
                                                    }
                                    });
                                }
                        });
                    }else{
                        console.log("No encontrado");
                        res.render('users/generarPagos');
                    }

                }

        });

        // db.query('SELECT * FROM sucursales ', (err, rows) => {
        //     if(err){
        //         res.render('error');
        //     }else{

        //         var dataSucursales = rows;

        //         db.query('SELECT * from pagos WHERE idDeudor =  ?', id, (err, rows) =>{
        //             if(err){
        //                 res.render('error');
        //             }else{
        //                 var dataPagos = rows;

        //                 res.render('users/generarPagos',
        //                     {
        //                         isAuthenticated : req.isAuthenticated(),
        //                         user : req.user,
        //                         data : dataSucursales,
        //                         pagos : dataPagos
        //                     });
        //                     //console.log(dataSucursales, dataPagos);

        //             }
        //         });

            
        //     }

        // });

    },

    postGenerarPago : function (req, res, next) {

        let datosPlanPagos = {
            FechaProximoPago : req.body.fechanueva,
            idSucursal : req.body.regiones,
            Meses : req.body.meses,
            Pago : req.body.pago,
            Intereses : req.body.intereses,
            Monto : req.body.monto,
            SaldosInsolutos : req.body.saldosI
        }

        var config =require('.././database/config');
        var db = mysql.createConnection(config);
        
        //conectamos la DB
        var IDpago = req.body.idpago;
        
        db.connect();
        db.query('UPDATE pagos SET ? WHERE idPago = ?', [datosPlanPagos, IDpago], (err, rows) => {
            if(err){
                res.render('error');
            }else{
                res.redirect('panel');
                console.log(datosPlanPagos);
            }
        });
        // console.log(datosPlanPagos);
        // res.redirect('consulta');
    },

    getCorreo : function (req, res, next){

        let id = req.params.idDeudor;
        console.log(id);
        var config =require('.././database/config');
        var db = mysql.createConnection(config);
        
        //conectamos la DB
        
        db.connect();

        db.query('SELECT * FROM pagos WHERE idDeudor= ? ', id, (err, rows) => {
            if(err){
                res.render('error');
            }else{

                var datosPag = rows;

                db.query('SELECT Nombre FROM deudores WHERE idDeudor= ? ', id, (err, rows) => {

                    if(err){
                        res.render('error');
                    }else{
        
                        var datosCliente = rows;
                        res.render('users/enviarCorreo',
                        {
                            isAuthenticated : req.isAuthenticated(),
                            user : req.user,
                            data : datosPag,
                            data2 : datosCliente
                        });
                        console.log(datosPag, datosCliente);
                    }

                });
                
            }

        });

        //res.render('users/enviarCorreo');

    },

    getPrueba : function(req, res, next){
        res.render('users/prueba',
        {
            isAuthenticated : req.isAuthenticated(),
            user : req.user
        });
    },

    postEnviarEmail : function(req, res, next){
        var asunto = req.body.asunto;
        var paraquien = req.body.email;
        var msm = req.body.descripcion;

                //create reusable transporter object using the default SMTP transport
                let transporter = nodemailer.createTransport({
                    service : 'gmail',
                    auth: {
                        user: 'admcompany7@gmail.com', // generated ethereal user
                        pass: 'SUPERADMIN10' // generated ethereal password
                    }
                });

                // setup email data with unicode symbols
                let mailOptions = {
                    from: '"Company RECUPERADORES DE CARTERAS 👻" <admcompany7@gmail.com>', // sender address
                    to: paraquien, // list of receivers
                    subject: 'Hello from Company ✔', // Subject line
                    text: msm, // plain text body
                    
                };

                // send mail with defined transport object
                transporter.sendMail(mailOptions, (error, response) => {
                    if (error) {
                        return console.log(error);
                    }
                    // console.log('Message sent: %s', info.messageId);
                    // // Preview only available when sending through an Ethereal account
                    // console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
                    console.log(response.message);
                    return res.redirect('panel');

                    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
                    // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
                });

                return res.redirect('panel');
       
    
    },


    getMes : function(req, res, next){
        let idA = req.params.idAgente;
        console.log(idA);
        var config =require('.././database/config');
        var db = mysql.createConnection(config);
        
        //conectamos la DB
        
        db.connect();

        db.query('SELECT * FROM sesiones WHERE Fecha LIKE ? AND idAgente = ?' , ['%12%', idA]
        , (err, rows) => {

            if(err){
                res.render('error');
            }else{

                console.log(rows);
                res.render('users/reportemes',
                {
                    isAuthenticated : req.isAuthenticated(),
                    user : req.user,
                    data : rows
                });
            }
        });
        // res.render('users/reportemes',
        // {
        //     isAuthenticated : req.isAuthenticated(),
        //     user : req.user
        // });
    },

    postMes : function (req, res, next) {

        let datosMes = {
            Fecha : req.body.fecha
        }

        var config =require('.././database/config');
        var db = mysql.createConnection(config);
        
        //conectamos la DB
        
        db.connect();
        db.query('SELECT * FROM capturasesion WHERE Fecha LIKE  ?', req.Fecha, (err, rows) => {
            if(err){
                res.render('error');
                console.log("No hay registros de esa fecha");
            }else{
                res.redirect('reportemes');
                console.log(datosMes);
            }
        });
        console.log(datosMes);
        res.redirect('panel');
    },


    getDia : function(req, res, next){
        let idAg = req.params.idAgente;
        console.log(idAg);

        var config =require('.././database/config');
        var db = mysql.createConnection(config);
        
        //conectamos la DB
        
        db.connect();
        //MODIFIQUE EL PARAMETRO
        db.query('SELECT * FROM sesiones WHERE Fecha LIKE ? AND idAgente = ?' , ['%06%', idAg]
        , (err, rows) => {

            if(err){
                res.render('error');
            }else{

                console.log(rows);
                res.render('users/reportedia',
                {
                    isAuthenticated : req.isAuthenticated(),
                    user : req.user,
                    data : rows
                });
            }
        });

        // res.render('users/reportedia',
        // {
        //     isAuthenticated : req.isAuthenticated(),
        //     user : req.user
        // });
    },


    postDia : function (req, res, next) {

        let datosDia = {
            Fecha : req.body.fecha
        }

        var config =require('.././database/config');
        var db = mysql.createConnection(config);
        
        //conectamos la DB
        
        db.connect();
        db.query('SELECT * FROM capturasesion WHERE Fecha LIKE  ?', datosDia.Fecha, (err, rows) => {
            if(err){
                res.render('error');
                console.log("No hay registros de esa fecha");
            }else{
                res.redirect('reportemes');
                console.log(datosDia);
            }
        });

        console.log(datosDia);
        res.redirect('panel');
    }

};